import pyb
''' @file EncoderDriver.py
encoder 
@author: neima yeganeh '''


class EncoderDriver:
    ''' This class implements a encoder driver for the
    ME405 board. '''

    def __init__ (self, pin1, pin2, timer):
        
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param pin1 A pin 
        @param pin2 A pins
        @param timer A pyb.Timer object to use a way to track the encoder value.
         '''
        self.pin1 = pin1
        self.pin2 = pin2
        self.timer = timer
        self.p1 = 0
        self.p2 = 0
        self.pos = 0
         

    def update (self):
        '''updates encoder position'''
        self.p2 = self.p1
        self.p1 = self.timer.counter()
        delt = self.get_delta()
        
        if (delt > 32768):
            #underflow
            self.pos -= (self.p2 + (65536 - self.p1))
        elif (delt < -32768):
            #overflow
            self.pos +=  (self.p1 +(65536 - self.p2))
        else:
            #normal
            self.pos += delt
            
    def get_position(self):
        '''returns encoder position'''
        return self.pos

    def set_position (self, p):
        ''' This method sets the encoder position. 
        @param p A signed integer holding the new position
        of the encoder '''
        self.pos = p
    def get_delta (self):
        '''finds change in encoder position'''
        return self.p1 - self.p2

if __name__ == '__main__':
   

    # Create the pin objects used for interfacing with the encoder driver
    pin_1CA = pyb.Pin (pyb.Pin.cpu.B6)
    pin_1CB = pyb.Pin (pyb.Pin.cpu.B7)
    pin_2CA = pyb.Pin (pyb.Pin.cpu.C6);
    pin_2CB = pyb.Pin (pyb.Pin.cpu.C7);

    # Create the timer object used for with encoder and sets the pins
    tim = pyb.Timer(4,prescaler=0, period=65535)
    tim.channel(1, pyb.Timer.ENC_A, pin=pin_1CA)
    tim.channel(2, pyb.Timer.ENC_B, pin=pin_1CB)
    
    tim2 = pyb.Timer(8,prescaler=0, period=65535)
    tim2.channel(1, pyb.Timer.ENC_A, pin=pin_2CA)
    tim2.channel(2, pyb.Timer.ENC_B, pin=pin_2CB)
    #creates encoder
    en = EncoderDriver(pin_1CA, pin_1CB, tim)
    #Smoke screen test
    if (en.get_position() != 0):
        print("error should be 0 orignial position")
    #final output
    while(1):
        en.update()
        print(en.pos)
    
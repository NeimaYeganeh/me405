def main():
    pin_1CA = pyb.Pin (pyb.Pin.cpu.C3)
    pin_1CB = pyb.Pin (pyb.Pin.cpu.A4)
    pin_2CA = pyb.Pin (pyb.Pin.cpu.C2)
    pin_2CB = pyb.Pin (pyb.Pin.cpu.C0)
    
    pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000)
    pin_EN2 = pyb.Pin (pyb.Pin.cpu. C1, pyb.Pin.OUT_PP)
    pin_IN12 = pyb.Pin (pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
    pin_IN22 = pyb.Pin (pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)

    # Create the timer object used for PWM generation
    tim2 = pyb.Timer(5, freq = 20000)

    # Create a motor object passing in the pins and timer
    moe1 = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    moe2 = MotorDriver(pin_EN2, pin_IN12, pin_IN22, tim2)
    
    
   
    i = IR(pin_1CA, pin_1CB, pin_2CA, pin_2CB)
    l = Line(i, moe1, moe2)
    #sample code to get to the location that is 3 intersections forward and 2 to the right
    l.runLine(3, 0)         # drive forward 3 intersection    
    l.rotateStart(-1)       # turn 90 degrees to the right
    l.runLine(2, 1)         # drive forward 2 more lines to get to the final location
    #sample of it returning home
    l.rotateStart(-1)       # rotate 90°
    l.rotateStart(-1)       # rotate another 90° to be facing oposite direction
    l.runLine(2, 1)
    l.rotateStart(1)
    l.runLine(3, 0)
if __name__ == '__main__':
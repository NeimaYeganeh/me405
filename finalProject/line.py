import pyb
import utime
from mo import MotorDriver
''' @file line.py
    @author: neima yeganeh '''
class Line:
    ''' This class implements a line logic driver for the
    ME405 board. '''
    def __init__ (self, ir, moe1, moe2):
        
        ''' Creates a motor class to make sense of infrared sensors and tell motors what to do.
        @param ir A ir object to sense lines.
        @param moe1 A motor object to comunicate with one of the motors.
        @param moe2 A motor object to comunicate with one of the motors.
        '''
        self.ir = ir
        self.moe1 = moe1
        self.moe2 = moe2
    def rotateStart(self, direction):
        '''This method rotates the bot 90 degrees in the direction specified.
        @param direction integer coraspon which way to turn. -1 or 1'''
        self.moe1.enable()
        self.moe2.enable()
        self.moe1.setduty(direction * -20)
        self.moe2.setduty(direction * 20)
        utime.sleep_ms(1400)
        self.moe1.set_duty(0)
        self.moe2.set_duty(0)
        
        self.moe1.disable()
        self.moe2.disable()        
    def runLine(self, numlines, typeLine):
        '''This method rotates the bot 90 degrees in the direction specified.
        @param numlines integer corasponding to the number of intersections the robot should pass
        @param typeLine integer corasponding to the type of intersections it was passing'''
        self.moe1.enable()
        self.moe2.enable()
        a = 3
        lineCount = 0
        while(1):
            utime.sleep_ms(10)
            b = self.ir.update()
            if (typeLine):
                a = self.ir.up(b, a)
            else:
                a = self.ir.upMiddle(b, a)
            if (a == 0 and b == numline):
                self.moe1.set_duty(0)
                self.moe2.set_duty(0)
                print("stop")
                self.moe1.disable()
                self.moe2.disable()
                break
            elif (a == 1):
                print("left")
                self.moe1.set_duty(20)
                self.moe2.set_duty(15)
            elif (a == 2):
                print("right")
                self.moe1.set_duty(15)
                self.moe2.set_duty(20)
            else:
                if (a == 0):
                    lineCount += 1
                print("straight")
                self.moe1.set_duty(20)
                self.moe2.set_duty(20)
                
                

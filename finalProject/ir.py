import pyb
import utime
from mo import MotorDriver
from line import Line
''' @file ir.py
    @author: neima yeganeh '''
class IR:
    ''' This class implements a infrared driver for the
    ME405 board. '''

    def __init__ (self,p1 , p2, p3, p4):
        
        ''' Creates a infrared driver. Used to get and understand logic fromt the infrared sensor.
        @param p1 A pyb.Pin object to use as I/O signal from the ir sensor.
        @param p2 A pyb.Pin object to use as I/O signal from the ir sensor.
        @param p3 A pyb.Pin object to use as I/O signal from the ir sensor.
        @param p4 A pyb.pin object to use as I/O signal from the ir sensor.
        '''
        
        self.p1 = pyb.ADC(p1)
        self.p2 = pyb.ADC(p2)
        self.p3 = pyb.ADC(p3)
        self.p4 = pyb.ADC(p4)
        
         

    def update (self):
        '''updates ir values and returns it as a tuple of booleans'''
        a = self.p1.read()
        b = self.p2.read()
        c = self.p3.read()
        d = self.p4.read()
        a = a < 4000
        b = b < 4000
        c = c < 4000
        d = d < 4000
        return (a, b, c, d)
    def upMiddle(self, a, b):
        '''This method returned the direction the bot should move based on the ir sensors. Used on lines with fully perpendicular lines.
        @param a tuple of 4 boolean values corasponding to IR values
        @param b the previous value retured from this function '''
        if (a == (False, False, False, False) or a == (True, False, False, False) or a == (True, True, False, False) or a == (False, False, True, True) or a == (False, False, False, True)): #on line end
            return 0
        elif (a == (True, True, True, True)):
            return b
        elif (a == (True, False, True, True) or a == (False, True, True, True) or a == (False, False, True, True) ):
            return 2
        elif (a == (True, True,False, True) or a == (True, True, True, False) or a == (True, True, False, False)):
            return 1
        elif (a == (True, False, False, True)):
            return 3
    def up(self, a, b):
        '''This method returned the direction the bot should move based on the ir sensors. Used on lines with partial perpendicular lines.
        @param a tuple of 4 boolean values corasponding to IR values
        @param b the previous value retured from this function '''
        
        if (a == (True, False, False, False) or a == (True, True, False, False) or a == (False, False, True, True) or a == (False, False, False, True)): #on line end
            return 0 #all on
        elif (a == (True, True, True, True)):
            return b #off line
        elif (a == (True, False, True, True)):
            return 2 #right
        elif (a == (True, True, False, True)):
            return 1 #left
        elif (a == (True, False, False, True)):
            return 3 #straight
            
if __name__ == '__main__':
    pin_1CA = pyb.Pin (pyb.Pin.cpu.C3)
    pin_1CB = pyb.Pin (pyb.Pin.cpu.A4)
    pin_2CA = pyb.Pin (pyb.Pin.cpu.C2)
    pin_2CB = pyb.Pin (pyb.Pin.cpu.C0)
    
    pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000)
    pin_EN2 = pyb.Pin (pyb.Pin.cpu. C1, pyb.Pin.OUT_PP)
    pin_IN12 = pyb.Pin (pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
    pin_IN22 = pyb.Pin (pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)

    # Create the timer object used for PWM generation
    tim2 = pyb.Timer(5, freq = 20000)

    # Create a motor object passing in the pins and timer
    moe1 = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    moe2 = MotorDriver(pin_EN2, pin_IN12, pin_IN22, tim2)
    
    
   
    i = IR(pin_1CA, pin_1CB, pin_2CA, pin_2CB)
    l = Line(i, moe1, moe2)
    
    #l.findLine(2, 1)
    #l.rotateStart(-1)
    #l.runLine(3, 0)
    l.rotateStart(-1)
    #l.runLine(1, 1)
    #l.rotateStart(-1)
    #l.rotate(-1)
    #l.runLine(1, 1)
    #l.rotate(1)
    #l.runline(2, 1)
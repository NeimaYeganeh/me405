import pyb
''' @file mo.py
    @author: neima yeganeh '''


class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
        and IN2_pin. '''
        print ('Creating a motor driver')
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        self.ch1 = timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.ch2 = timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)

    def enable (self):
        '''enables motor'''
        self.EN_pin.high ()

    def disable (self):
        '''disables motor'''
        self.EN_pin.low ()

    def set_duty (self, duty):
        '''this method sets the duty cycle to be sent to the motor to give level. Positive values cause effort in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        if(duty >= 0):
            ch1.pulse_width_percent(duty)
            self.IN2_pin.pulse_width_percent(0)
            
        else:
            ch2.pulse_width_percent(abs(duty))
            self.IN1_pin.pulse_width_percent(0)
            

    
    
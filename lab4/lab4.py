import pyb
import ustruct
import utime

class sensor:
    ''' This class implements an i2c sensor driver for the
    ME405 board. '''

    def __init__ (self, i2c):
        
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param i2c A pyb.i2c object to used to read sensor data.
        '''
        self.i2c = i2c
        self.DEV_ADDR = 0x28
         

    def enable (self):
        '''enables i2c'''
        self.i2c.start()
    def disable (self):
        '''disables i2c'''
        self.i2c.stop()
    def set_mode (self, mode): #NDOF mode = 0x0c
        '''sets mode
        @param mode an integer representing the hex mode'''
        self.i2c.mem_write(mode, self.DEV_ADDR, 0x3d)    #write new mode NDOF
        
    def check_cal (self):
        '''returns IMU calibration status'''
        f0 = int(0b11000000)
        f1 = int(0b00110000)
        f2 = int(0b00001100)
        f3 = int(0b00000011)
        cal_data = self.i2c.mem_read(1, self.DEV_ADDR, 0x35) #reads full calibration
        cal_data = int.from_bytes(cal_data, "little")
        return (((cal_data & f0) >> 6) & 3, ((cal_data & f1)) >> 4, ((cal_data& f2)) >> 2, cal_data & f3)
    def get_euler_angles(self):
        '''Get the orientation as three Euler angles and return them as a tuple'''
        returns the 
        euler_head_lsb = self.i2c.mem_read(6, self.DEV_ADDR, 0x1A)
        euler_values = ustruct.unpack('<hhh',euler_head_lsb)
        f = tuple(t/16.0 for t in euler_values)
        return f
    def get_angular_velocities(self):
        '''Get the three angular velocities and return them as a tuple'''
        gyro_x_lsb = self.i2c.mem_read(6, self.DEV_ADDR, 0x14)
        gyro_values = ustruct.unpack('<hhh',gyro_x_lsb)
        f = tuple(t/100.0 for t in gyro_values)
        return f

if __name__ == '__main__':
    i2c = pyb.I2C(1,pyb.I2C.MASTER)
    s = sensor(i2c)
    while(1):
        utime.sleep_ms(3000)
        print("Eul Ang: ", s.get_euler_angles())
        print("Eul Vel: ", s.get_angular_velocities())
        print("Calib:   ", s.check_cal())
    
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 11:56:54 2020

@author: neimayeganeh
"""

''' @file main.py Fibonacci Program for lab 0 '''
## Fibinacci sequence
def fib (idx):
   ''' This method calculates a Fibonacci number corresponding to a specified index recursively.
   @param idx An integer specifying the index of the desired Fibonacci number.
   @return the Fibinacci value at index idx. '''
   
   if idx == 0:
      return 0
   elif idx == 1:
      return 1
   else:
      return fib(idx-2) + fib(idx-1)

   
   
   


if __name__ == '__main__':
   ## integer containing either 1 for the first fib number or 0 for any other 
   first = 1
   
   ## string containing fib index or EXIT if not the first index
   answer = ""
   while (1):
      if first:
         answer = input('Input index for desired Fibonacci number: ')
         if answer.isdigit():
            print ('Fibonacci index {} = {}.'.format( (int(answer)), fib((int(answer)))))
            first = 0
         else:
            print('Index needs to be a positive integer or 0')
      else:
         answer = input('Input index for desired Fibonacci number or Type "EXIT" to end: ')
         if answer.isdigit():
            print ('Fibonacci index {} = {}.'.format( (int(answer)), fib((int(answer)))))
            first = 0
         else:
            if answer == 'EXIT':
               break;
            else:
               print('Index needs to be a positive integer or 0, or type "EXIT" to quit')
         
    

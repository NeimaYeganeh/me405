import pyb
''' @file encoder.py
    @author: neima yeganeh '''


class enDriver:
    ''' This class implements an encoder driver for the
    ME405 board. '''

    def __init__ (self, pin1, pin2, timer):
        
        ''' Creates a encoder driver by initializing timer and pins
        @param pin1 A pin 
        @param pin2 A pins
        @param timer A pyd.Timer object to use a way to
        track the encoder value. '''
        self.pin1 = pin1
        self.pin2 = pin2
        self.timer = timer
        self.p1 = 0
        self.p2 = 0
        self.pos = 0
         

    def update (self):
        '''updates encoder position'''
        self.p2 = self.p1
        self.p1 = self.timer.counter()
        delt = self.get_delta()
        
        if (delt > 32768):
            #underflow
            self.pos -= (self.p2 + (65536 - self.p1))
        elif (delt < -32768):
            #overflow
            self.pos +=  (self.p1 +(65536 - self.p2))
        else:
            #normal
            self.pos += delt
            
    def get_position(self):
        '''returns position'''
        return self.pos

    def set_position (self, p):
        '''This method sets the ecoder position
            @param p A signed integer holding the new position
            of the encoder'''
        self.pos = p
    def get_delta (self):
        '''finds change in encoder position'''
        return self.p1 - self.p2
    # testing
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    # Create the pin objects used for interfacing with the motor driver
    #pin_1CA = pyb.Pin (pyb.Pin.cpu.B6)
    #pin_1CB = pyb.Pin (pyb.Pin.cpu.B7)
    #pin_2CA = pyb.Pin (pyb.Pin.cpu.C6);
    #pin_2CB = pyb.Pin (pyb.Pin.cpu.C7);

    # Create the timer object used for PWM generation
    #tim = pyb.Timer(4,prescaler=0, period=65535)
    #tim.channel(1, pyb.Timer.ENC_A, pin=pin_1CA)
    #tim.channel(2, pyb.Timer.ENC_B, pin=pin_1CB)
    
    #tim2 = pyb.Timer(8,prescaler=0, period=65535)
    #tim2.channel(1, pyb.Timer.ENC_A, pin=pin_2CA)
    #tim2.channel(2, pyb.Timer.ENC_B, pin=pin_2CB)
    #en = EncoderDriver(pin_1CA, pin_1CB, tim)
    #if (en.get_position() != 0):
    #    print("error should be 0 orignial position")
    #while(1):
    #    en.update()
    #    print(en.pos)
        
    

    # Create a motor object passing in the pins and timer
    #moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

    # Enable the motor driver
    #moe.enable()
    

    # Set the duty cycle to 10 percent
    #moe.enable()
    #moe.set_duty(10)
    # moe.disable()
    
    
    
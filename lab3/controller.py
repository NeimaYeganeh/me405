import pyb
''' @file controller.py
    @author: neima yeganeh '''


class Controller:
    ''' This class implements a controller driver for the
    ME405 board.'''

    def __init__ (self, setPoint, kpp):
        ''' Creates a controller driver by initializing a setpoint and kp.
        @param setPoint A integer representing the desired end point
        @param kpp A float representing the kp gain value
        '''
        self.setPoint = setPoint # desired location 1000, .1
        self.k = kpp            # measure location and sub setPoint = error sig
                                 # larger error harder push
                                 # errorsig * const Kp = actuation sig a duty cycle in percent
                                 # send actuation sig to motor drive
       
    #takes pos-position 
    #returns motor val
    def update (self, pos):
        '''return desired motor duty
        @param pos An integer representing the ecoder position'''
        errSig = self.setPoint - pos
        actSig = errSig * self.k
        return actSig


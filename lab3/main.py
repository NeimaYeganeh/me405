import pyb
from encoder import enDriver
from motor import MotorDriver
import utime
from controller import Controller
''' @file main.py
    @author: neima yeganeh '''

def printLi(l1, l2):
    '''prints out list of times and positions
    @param l1 a list containing integers representing time in ms
    @param l2 a list containing integers representing encoder distance'''
    i = 0
    print("time(ms)\tposition(ticks)")
    while (i < len(l1)):
        print(l1[i], "\t", l2[i])
        i += 1
def runKp(kp):
    ''' Runs an instance of the cotroller with given kp
    @param kp a float representing kp gain'''
    pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
    tim = pyb.Timer(3, freq = 20000)
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    pin_1CA = pyb.Pin (pyb.Pin.cpu.B6)
    pin_1CB = pyb.Pin (pyb.Pin.cpu.B7)
    tim = pyb.Timer(4,prescaler=0, period=65535)
    tim.channel(1, pyb.Timer.ENC_A, pin=pin_1CA)
    tim.channel(2, pyb.Timer.ENC_B, pin=pin_1CB)
    en = enDriver(pin_1CA, pin_1CB, tim)
    
    moe.enable()
    
    setP = 10000
    
    con = Controller(setP, kp)
    start = 0
    i = 0
    time = 0
    a = 5
    old =''
    l1 = []
    l2 = []
    while(1):
        utime.sleep_ms(10)
        if (i == 0):
            start =  utime.ticks_ms()
            time = 0
            i = 1
        else:
            time += (utime.ticks_ms()-start)
        
        encoderval = en.update()
        duty = con.update(en.pos)
        if(en.pos == old):
            break
        old = en.pos
        l2.append(en.pos)
        l1.append(time)
        moe.set_duty(duty)
        
        a -= 1
    moe.set_duty(0)
    moe.disable()
    printLi(l1, l2)
    
    print("done")
    
def is_float(str):
    '''returns true if argument is float
    @parameter str is an uknown type'''
    try:
        float(str)
        return True
    except ValueError:
        return False
def main():
    '''This runs step response test'''
    while(1):
        out = input("enter kp value or type 'EXIT' to quit: ")
        if (out.isdigit()):
            print("Starting step test")
            out = int(out)
            runKp(out)
        elif (is_float(out)):
            print("Starting step test")
            out = float(out)
            runKp(out)
        elif (out == 'EXIT'):
            break
        else:
            print("please try again")
    return 0
    

if __name__ == "__main__":
    main()
